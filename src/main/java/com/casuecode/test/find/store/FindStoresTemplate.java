package com.casuecode.test.find.store;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.casuecode.test.find.store.dao.StoresDAO;
import com.casuecode.test.find.store.model.StoreModel;

/**
 * template class that handles the rest calls to perfrom all STORE operations
 * create/update/delete/find
 * 
 * @author SohanB
 *
 */

@Path("/api")
public class FindStoresTemplate {

    private static final long   serialVersionUID = 230395165235188374L;
    private final static Logger logger           = Logger.getLogger(FindStoresTemplate.class);

    ApplicationContext          context          = new ClassPathXmlApplicationContext("beans.xml");
    StoresDAO                   storesDAO        = (StoresDAO) context.getBean("storesDAO");

    @GET
    @Path("{storename}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStore(@PathParam("storename") String storename, @Context UriInfo uriInfo,
            String content) throws StoreRepoException {
        logger.info("Getting store details of " + storename);
        StoreModel strObj = storesDAO.getStore(storename);
        if (strObj != null)
            return Response.status(201).entity(storesDAO.getStore(storename)).build();
        else
            return Response.status(201).entity("Unable to find storename " + storename).build();

    }

    @POST
    @Path("/add-store")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addStore(StoreModel storesdetails) throws StoreRepoException {
        logger.info("Adding Store detials");
        boolean strObj = storesDAO.addStore(storesdetails.getZipCode(), storesdetails.getStoreName(),
                storesdetails.getLocation(), storesdetails.getMiles());
        if (strObj)
            return Response.status(201).entity("Store  Details Succesfully Added").build();
        else
            return Response.status(201).entity("Failed to Add store details").build();
    }

    @PUT
    @Path("/update-store")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateStore(StoreModel updateStoresdetails) throws StoreRepoException {
        logger.info("Updating store details");
        boolean strObj = storesDAO.updateStore(updateStoresdetails);
        if (strObj)
            return Response.status(201).entity("Store  Details Succesfully Update").build();
        else
            return Response.status(201).entity("Failed to Update Store details").build();

    }

    @DELETE
    @Path("/delete-store")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteStore(StoreModel deleteStore) throws StoreRepoException {
        logger.info("Deleting store with storename : " + deleteStore.getStoreName());
        boolean strObj = storesDAO.deleteStore(deleteStore);
        if (strObj)
            return Response.status(201).entity("Store  Details Succesfully Delete").build();
        else
            return Response.status(201).entity("Failed to Delete Store details").build();

    }

    @POST
    @Path("/find-store")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findStores(StoreModel storesdetails) throws StoreRepoException {
        logger.info("Finding details of store : " + storesdetails.getStoreName() + "within "
                + storesdetails.getMiles() + " miles");
        List<StoreModel> strObj = null;
        if (storesdetails.getZipCode() != null)
            strObj = storesDAO.findStoresLocation(storesdetails.getStoreName(), storesdetails.getZipCode(),
                    storesdetails.getMiles());
        else
            strObj = storesDAO.findStoresLocation(storesdetails.getStoreName(), null,
                    storesdetails.getMiles());
        if (strObj != null && strObj.size() > 0)
            return Response.status(201).entity(strObj).build();
        else
            return Response.status(201).entity("No stores found within " + storesdetails.getMiles() + "miles")
                    .build();
    }
}
