package com.casuecode.test.find.store.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.casuecode.test.find.store.model.StoreModel;

/**
 * mapper class to get storelocation details
 * @author SohanB
 *
 */
public class StoreLocationMapper implements RowMapper<StoreModel> {

    public StoreModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        StoreModel strloc = new StoreModel();
        strloc.setStoreName(rs.getString("storeName"));
        strloc.setZipCode(rs.getString("zipCode"));
        strloc.setLocation(rs.getString("location"));
        strloc.setMiles(rs.getInt("miles"));
        return strloc;
    }

}
