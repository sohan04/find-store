package com.casuecode.test.find.store.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.casuecode.test.find.store.model.StoreModel;

/**
 * mapper class to get store details
 * @author SohanB
 *
 */
public class StoreMapper implements RowMapper<StoreModel> {

    public StoreModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        StoreModel str = new StoreModel();
        str.setZipCode(rs.getString("zipCode"));
        str.setStoreName(rs.getString("storeName"));
        str.setLocation(rs.getString("location"));
        str.setMiles(rs.getInt("miles"));
        return str;
    }

}
