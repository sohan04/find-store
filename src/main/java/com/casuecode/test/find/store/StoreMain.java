package com.casuecode.test.find.store;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.casuecode.test.find.store.dao.StoresDAO;
import com.casuecode.test.find.store.model.StoreModel;

public class StoreMain {

    public static void main(String[] args) throws StoreRepoException {
        ApplicationContext context = 
                new ClassPathXmlApplicationContext("beans.xml");
        StoresDAO storesDAO = (StoresDAO) context.getBean("storesDAO");
        
        System.out.println("------Records Creation--------" );
        StoreModel strNm = storesDAO.getStore("mac-d");
        System.out.println("zipCode " +strNm.getZipCode());
        System.out.println("sotrName " +strNm.getStoreName());
        System.out.println("zipCode " +strNm.getLocation());
        System.out.println("miles " +strNm.getMiles());

    }
}
