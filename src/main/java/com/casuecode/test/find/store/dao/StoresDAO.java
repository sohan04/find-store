package com.casuecode.test.find.store.dao;

import java.util.List;


import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.casuecode.test.find.store.IStores;
import com.casuecode.test.find.store.StoreRepoException;
import com.casuecode.test.find.store.mapper.StoreLocationMapper;
import com.casuecode.test.find.store.mapper.StoreMapper;
import com.casuecode.test.find.store.model.StoreModel;

/**
 * DAO class to add/update/delete/retrive stores information from database
 * @author SohanB
 *
 */
public class StoresDAO implements IStores {

    private final static Logger logger                    = Logger.getLogger(StoresDAO.class);

    private static final String select_storename          = "select * from stores where storename = ?";
    private static final String add_store                 = "insert into StoreModel (zipCode, storename,location,miles) values (? ,?, ? ,?)";
    private static final String update_store              = "update stores set storename=? ,location=? where zipCode=?";
    private static final String delete_store              = "delete from stores where zipCode=? and storename=? ";
    private static final String find_store_witin_zipcodes = "select zipcode,storename,location,miles from stores where zipcode=? and storename=? and miles <= ?  ";
    private static final String find_all_stores           = "select zipcode,storename,location,miles from stores where storename=? and miles <= ?  ";
    private DataSource          _dataSource;
    private JdbcTemplate        jdbcTemplateObject;

    private static StoresDAO    instance                  = new StoresDAO();

    public void setDataSource(DataSource dataSource) {
        this._dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    private StoresDAO() {

    }

    public static StoresDAO getInstance() {
        return instance;
    }

    public StoreModel getStore(String storename) throws StoreRepoException {
        logger.info("Getting store information from database");
        StoreModel str = null;
        try {
            str = jdbcTemplateObject.queryForObject(select_storename, new Object[] { storename },
                    new StoreMapper());
        } catch (EmptyResultDataAccessException era) {
            System.out.println("Cannot find result");
        }

        return str;
    }

    public List<StoreModel> getStoresList() throws StoreRepoException {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean addStore(String zipcode, String storeName, String location, Integer miles)
            throws StoreRepoException {
        logger.info("Adding store information to database");
        int iresponse = 0;
        try {
            iresponse = jdbcTemplateObject.update(add_store, zipcode, storeName, location, miles);
        } catch (EmptyResultDataAccessException era) {
            throw new StoreRepoException("result set was emtpy", era);
        } catch (DuplicateKeyException dke) {
            throw new StoreRepoException("cannot insert with duplicate entries", dke);
        }
        if (iresponse == 1)
            return true;
        else
            return false;

    }

    public boolean updateStore(StoreModel str) throws StoreRepoException {
        logger.info("Updating store information in database");
        int iresponse = 0;
        try {
            iresponse = jdbcTemplateObject.update(update_store, str.getStoreName(), str.getLocation(),
                    str.getZipCode());
        } catch (EmptyResultDataAccessException era) {
            throw new StoreRepoException("Cannot update emtpy with result set ", era);
        } catch (DuplicateKeyException dke) {
            throw new StoreRepoException("cannot update with duplicate entries", dke);
        } catch (BadSqlGrammarException bge) {
            throw new StoreRepoException("cannot update with emtpy or bad entries", bge);
        }
        if (iresponse == 1)
            return true;
        else
            return false;

    }

    public boolean deleteStore(StoreModel str) throws StoreRepoException {
        logger.info("Deleting store details from database");
        int iresponse = 0;
        try {
            iresponse = jdbcTemplateObject.update(delete_store, str.getZipCode(), str.getStoreName());
        } catch (EmptyResultDataAccessException era) {
            throw new StoreRepoException("Cannot delete with emtpy result set ", era);
        } catch (BadSqlGrammarException bge) {
            throw new StoreRepoException("cannot delete with emtpy or bad entries", bge);
        }
        if (iresponse == 1)
            return true;
        else
            return false;

    }

    public List<StoreModel> findStoresLocation(String storeName, String zipCode, Integer miles)
            throws StoreRepoException {
        logger.info("Trying to find store details of " + storeName + "within " + miles + " miles");
        List<StoreModel> iresponse = null;
        try {
            if (zipCode != null) {
                logger.debug(
                        "trying to find all stores within " + miles + " miles with zipcode : " + zipCode);
                Object[] params = { zipCode, storeName, miles };
                iresponse = jdbcTemplateObject.query(find_store_witin_zipcodes, params,
                        new StoreLocationMapper());
            } else {
                logger.debug("zipcode sent was emtpy, trying to find all stores within " + miles + " miles");
                Object[] params = { storeName, miles };
                iresponse = jdbcTemplateObject.query(find_all_stores, params, new StoreLocationMapper());
            }
        } catch (EmptyResultDataAccessException era) {
            throw new StoreRepoException("Cannot find with emtpy result set", era);
        } catch (BadSqlGrammarException bge) {
            throw new StoreRepoException("cannot find result with emtpy or bad entries", bge);
        }
        return iresponse;

    }

}
