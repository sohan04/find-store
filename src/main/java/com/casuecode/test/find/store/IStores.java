package com.casuecode.test.find.store;

import java.util.List;

import javax.sql.DataSource;

import com.casuecode.test.find.store.model.StoreModel;

/**
 * interface to define all required api's to be implemented by template class
 * 
 * @author SohanB
 *
 */
public interface IStores {

    /**
     * set database instance configured in application-context
     * 
     * @param ds -datasource instance
     */
    public void setDataSource(DataSource ds);

    /**
     * get details of specific store
     * 
     * @param storename
     * @return store details
     * @throws StoreRepoException
     */
    public StoreModel getStore(String storename) throws StoreRepoException;

    /**
     * list all the store names
     * 
     * @return list of store names
     */
    public List<StoreModel> getStoresList() throws StoreRepoException;

    /**
     * creates new store entry
     * 
     * @param storeName -names of store
     * @param zipcode -area in which it is located
     * @param location -exact location of store
     * @param miles -how far it is located
     * @return true if successfully created
     * @throws StoreRepoException
     */
    public boolean addStore(String storeName, String zipcode, String location, Integer miles)
            throws StoreRepoException;

    /**
     * updated store details
     * 
     * @param store - storename and location can be updated with provided zipcode
     * @return true if successfully updated
     * @throws StoreRepoException
     */
    public boolean updateStore(StoreModel store) throws StoreRepoException;

    /**
     * deletes specific store entry
     * 
     * @param store - storename and zipcode params to delete specific store
     * @return true if successfully deleted
     * @throws StoreRepoException
     */
    public boolean deleteStore(StoreModel store) throws StoreRepoException;

    /**
     * finds stores within specified miles range with/without zipcode
     * 
     * @param storeName - storename to find
     * @param zipcode (Optional) -area in which store must be. If zipcode not specified it will
     *        return will all the stores within X miles with location
     * @param miles -ranges/how fas store can be located
     * @return exact location name of stores with all details
     * @throws StoreRepoException
     */
    public List<StoreModel> findStoresLocation(String storeName, String zipcode, Integer miles)
            throws StoreRepoException;

}
