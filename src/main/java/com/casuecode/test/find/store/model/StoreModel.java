package com.casuecode.test.find.store.model;

/**
 * store data model class
 * @author SohanB
 *
 */
public class StoreModel {

    public String  zipCode   = null;
    public String  storeName = null;
    public Integer miles     = 0;
    public String  location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Integer getMiles() {
        return miles;
    }

    public void setMiles(Integer miles) {
        this.miles = miles;
    }

}
