package com.casuecode.test.find.store;

/**
 * wrapper class to handle store application exceptions
 * @author SohanB
 *
 */
@SuppressWarnings("serial")
public class StoreRepoException extends Exception{

	public StoreRepoException() {
        super("Find Store api exception");
    }
    
    public StoreRepoException(String msg) {
        super(msg);
    }

    public StoreRepoException(String msg, Throwable cause) {
        super(msg, cause);
    }
	
}
