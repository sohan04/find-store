# README #



### What is this repository for? ###

# Quick Overview #
 * rest-api application to create,update delete store details 
 * Here store is referred to any restaurant,drug shop or general store
 * This application also provides rest-api to find stores location within 'X' miles from given zipCodes. Incase if zipCodes are not sent by user, it will find all the stores within 'X' miles and return result with details.

 **Note :** 
  
  * Output displayed will be in JSON format
  * This application does not accept the user location. Right now it is assumed that user know it's location and find out the stores nearby looking at store location.


# Technologies #
 * Core JAVA/J2EE
 * Spring with Spring JDBC template
 * Mysql as database
 * Jersey framework
 * maven
 * Jetty

## Assumptions ##
* It is assumed to find a specific store user will send the required information like, storename to find, area in which user is looking for zipcode (optional) and the range of location it should find.

## Prerequisites ##
* Jetty web server running - we will deploy our application into jetty. If you dont have jetty server you can download from [jetty-downloads](http://www.eclipse.org/jetty/download.html) 
* Mysql database running on **localhost:3306**
* Postman tool to fire the rest request. One can also use curl.
* find-store project is cloned on your local repository

## Setup application ##

### Summary of set up ###
We will setup application by following below instructions:

* **Configuration**
 
 1. Configure log4j.properties file - If you need to change the default settings of lo4j.properties located in project at* /find.store/src/main/resources*
     from this you will also need to change the **log4j.appender.file.File*** path
 2. Also if you mysql server is running on some other **host:port** then please change the **beans.xml** in */find.store/src/main/resources* with your own mysql **host:port**. Once the application is build it cannot be re-configured. (Configuring these properties currently is out of scope)

* **Dependencies**

  1. Jetty web server running

  2. mysql Db running

* **Database configuration**

  Before we start the application let's create database and schema where we will store the store information
  
  Connect to your Mysql DB
  Execute following : 
    
```
#!shell

mysql> create database stores;

```
   Create Table
     
```
#!sql

CREATE TABLE stores(  
   zipCode VARCHAR(20) NOT NULL,
   storename  VARCHAR(20) NOT NULL,
   location    VARCHAR(20) NOT NULL,
   miles  INT NOT NULL   ,
   PRIMARY KEY (zipCode,storename,location)
);
```
   You can also load the sample data provided in *<project_dir>/main/resources/sample_inserts.dml*. Following are sample insert statements.

```
#!sql

insert into Stores (zipCode, storename,location,miles) values("BOSTON MA 02201-1020","mac-d","BOSTON CITY HALL",10);
insert into Stores (zipCode, storename,location,miles) values("BOSTON MA 02201-1020","mac-d","CITY RECORDS",5);

insert into Stores (zipCode, storename,location,miles) values("BOSTON MA 02201-1020","pizza hut","Link RECORDS",4);
insert into Stores (zipCode, storename,location,miles) values("BOSTON MA 02201-2006","seven eleven","REGISTRY OF BIRTHS & DEATHS Office",3);
insert into Stores (zipCode, storename,location,miles) values("BOSTON MA 02201-2006","seven eleven","ELECTIONS Office",2);
insert into Stores (zipCode, storename,location,miles) values("BOSTON MA 02202-2222","seven eleven","Baking Street",9);
```



* **Compiling and creating web application (war file)**

  Clone the project to your local directory. Go to your project directory and run below command. It will create the** findstores.war** file in your *<project_dir>/target* folder
```
#!shell

mvn clean install
```

* **Deployment instructions**

  Copy WAR file and paste into *<jetty_webapp_server>/webapps* folder

### Executing the rest api's ###
Make sure your application is ready and listening on your localhost:<port>. I have used POSTMAN tool to perform following operations

* **add store details**

    **Rest-api** : http://localhost:8080/findstores/api/add-store

    **Type** : POST

    **JSON body** :
  
```
#!json
{
	"zipCode":"02201-1020",
	"storeName" :"mac-d",
  	"miles":8,
	"location":"BOSTON CITY HALL"
}

```

* **update store details**

    **Rest-api** : http://localhost:8080/findstores/api/update-store

    **Type** : PUT

    **JSON body** :
  
```
#!json
{
	"zipCode":"BOSTON MA 02201-1020",
	"storeName" :"new-mac-d",
	"location": "SB Road"
}

```

* **delete store details**

    **Rest-api** : http://localhost:8080/findstores/api/delete-store

    **Type** : DELETE

    **JSON body** :
  
```
#!json
{
	"zipCode":"BOSTON MA 02201-1020",
	"storeName" :"new-mac-d"
}
```

* **get store details**

    **Rest-api** : http://localhost:8080/findstores/api/<store-name>

                   http://localhost:8080/findstores/api/mac-d

    **Type** : GET


* **find store details within X-miles**

    **Rest-api** : http://localhost:8080/findstores/api/find-store

    **Type** : POST

    **JSON body** :
  
```
#!json
{
	"zipCode":"BOSTON MA 02201-1021",
	"storeName" :"seven eleven",
	"miles": 9
}

```

* **find all stores details within X-miles**

    **Rest-api** : http://localhost:8080/findstores/api/find-store

    **Type** : POST

    **JSON body** :
  
```
#!json
{	
	"storeName" :"seven eleven",
	"miles": 9
}

```

# Future Scope #
* Make Database and log4j properties configurable
* Add authentication and authorization layer  for each rest-call request. Can be done using simple BASIC authentication in first release.
* We will accept the user location to locate the stores nearby it's location. So the Database will have extra entry **userLocation** where all stores of one location goes under respective zipcode for specific stores.